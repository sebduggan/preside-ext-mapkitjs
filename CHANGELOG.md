# Changelog

## v0.4.0

* MapkitSnapshot for building and generating snapshots
* Add helpers for token and snapshot

## v0.3.1

* Change variable location of tokenUrl

## v0.3.0

* Implement JWT creation and ES256 signing
* Add Snapshot service

## v0.2.0

* Initial minimal release