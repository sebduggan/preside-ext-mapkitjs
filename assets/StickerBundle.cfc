/**
 * Sticker bundle configuration file. See: http://sticker.readthedocs.org/
 */

component output=false {

	public void function configure( bundle ) output=false {
		bundle.addAssets(
			  directory   = "/js/"
			, match       = function( path ){ return ReFindNoCase( "_[0-9a-f]{8}\..*?\.min.js$", arguments.path ); }
			, idGenerator = function( path ) {
				return ListDeleteAt( path, ListLen( path, "/" ), "/" ) & "/";
			}
		);

		bundle.addAssets(
			  directory   = "/css/"
			, match       = function( path ){ return ReFindNoCase( "_[0-9a-f]{8}\..*?\.min.css$", arguments.path ); }
			, idGenerator = function( path ) {
				return ListDeleteAt( path, ListLen( path, "/" ), "/" ) & "/";
			}
		);

		bundle.addAsset( id="mapkit-js", url="https://cdn.apple-mapkit.com/mk/5.x.x/mapkit.js" );
		bundle.asset( "/js/mapkit/mapkitInit/" ).dependsOn( "mapkit-js" );
	}
}