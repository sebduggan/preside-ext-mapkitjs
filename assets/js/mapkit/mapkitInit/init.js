mapkit.init( {
	authorizationCallback: function( done ) {
		var oReq = new XMLHttpRequest();
		oReq.addEventListener( "load", function(){
			done( this.responseText );
		} );
		oReq.open( "GET", cfrequest.mapkit.tokenUrl );
		oReq.send();
	}
} );