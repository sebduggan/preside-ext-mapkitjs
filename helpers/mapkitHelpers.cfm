<cffunction name="mapkitToken" access="public" returntype="string" output="false">
	<cfreturn getSingleton( "MapkitAuthenticationService" ).getJwt() />
</cffunction>

<cffunction name="mapkitSnapshot" access="public" returntype="MapkitSnapshot" output="false">
	<cfreturn getSingleton( "MapkitSnapshotService" ).newSnapshot( argumentCollection=arguments ) />
</cffunction>
