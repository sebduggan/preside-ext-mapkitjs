/**
 * @presideService
 * @singleton
 */
component {

	property name="jwt"           inject="jwt@jwtcfml";
	property name="encodingUtils" inject="encodingUtils@jwtcfml";

// CONSTRUCTOR
	/**
	 *
	 */
	public any function init() {
		variables.jss         = createObject( "java", "java.security.Signature" );
		variables.javaVersion = _getJavaVersion();
		variables.legacyJava  = variables.javaVersion < 11;

		return this;
	}

	public string function getJwt() {
		var event          = $getRequestContext();
		var prc            = event.getCollection( private=true );
		var domain         = prc._site.domain ?: "";
		var mapkitSettings = $getPresideCategorySettings( "mapkitjs" );
		var headers        = { kid=mapkitSettings.key_id };
		var payload        = {
			  iss    = mapkitSettings.team_id
			, iat    = Now()
			, exp    = DateAdd( "n", 20, Now() )
		};

		if ( Len( domain ) ) {
			payload.origin = domain;
		}

		return jwt.encode( payload, mapkitSettings.private_key, "ES256", headers );
	}

	public string function signES256( message, key ) {
		var algorithm   = variables.legacyJava ? "SHA256withECDSA" : "SHA256withECDSAinP1363Format";
		var jssInstance = variables.jss.getInstance( algorithm );
		var parsedKey   = encodingUtils.parsePEMEncodedKey( arguments.key );

		jssInstance.initSign( parsedKey );
		jssInstance.update( charsetDecode( message, "utf-8" ) );

		var sig = jssInstance.sign();
		if ( variables.legacyJava ) {
			sig = encodingUtils.convertDERtoP1363( sig, algorithm );
		}

		return encodingUtils.binaryToBase64Url( sig );
	}


// PRIVATE HELPER FUNCTIONS
	private numeric function _getJavaVersion() {
		var javaVersion = createObject( "java", "java.lang.System" ).getProperty( "java.version" );
		if ( javaVersion.startswith( "1." ) ) {
			return int( listGetAt( javaVersion, 2, "." ) );
		}
		return int( listFirst( javaVersion, "." ) );
	}
}