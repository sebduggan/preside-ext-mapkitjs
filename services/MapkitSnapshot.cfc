component accessors=true {
	property name="width"       type="numeric" default=0;
	property name="height"      type="numeric" default=0;
	property name="retina"      type="boolean" default=false;
	property name="center"      type="string"  default="";
	property name="zoom"        type="numeric" default="";
	property name="span"        type="string"  default="";
	property name="type"        type="string"  default="";
	property name="darkMode"    type="boolean" default=false;
	property name="hidePoi"     type="boolean" default=false;
	property name="language"    type="string"  default="";

	property name="annotations" type="array";
	property name="overlays"    type="array";

	public MapkitSnapshot function init( required any snapshotService ){
		_setSnapshotService( arguments.snapshotService );

		return this;
	}

	public string function getUrl() {
		return _getSnapshotService().buildSnapshotUrl( argumentCollection=getParams() );
	}

	public any function getBinary() {
		var snapshotUrl  = getUrl();
		return _getSnapshotService().getSnapshotBinary( snapshotUrl );
	}

	public struct function getParams() {
		var params = {};

		params.size   = getWidth() & "x" & getHeight();
		params.center = getCenter();

		if ( Val( getZoom() ) ) {
			params.z = getZoom();
		}
		if ( getRetina() ) {
			params.scale = 2;
		}
		if ( Len( getSpan() ) && getCenter() != "auto" ) {
			params.spn = getSpan();
		}
		if ( Len( getType() ) ) {
			params.t = getType();
		}
		if ( getDarkMode() ) {
			params.colorScheme = "dark";
		}
		if ( getHidePoi() ) {
			params.poi = 0;
		}
		if ( Len( getLanguage() ) ) {
			params.lang = getLanguage();
		}
		if ( ArrayLen( getAnnotations() ) ) {
			params.annotations = getAnnotations();
		}
		if ( ArrayLen( getOverlays() ) ) {
			params.overlays = getOverlays();
		}

		return params;
	}

	public array function getAnnotations() {
		return variables.annotations ?: [];
	}
	public array function getOverlays() {
		return variables.overlays ?: [];
	}

	public MapkitSnapshot function addAnnotation(
		  required string point
		,          string color       = ""
		,          string markerStyle = ""
		,          string glyphText   = ""
	) {
		var annotation = {};

		for( var key in [ "point", "color", "markerStyle", "glyphText" ] ) {
			if ( Len( arguments[ key ] ) ) {
				annotation[ key ] = arguments[ key ];
			}
		}

		variables.annotations = variables.annotations ?: [];
		ArrayAppend( variables.annotations, annotation );

		return this;
	}

	public MapkitSnapshot function addOverlay(
		  required array   points
		,          string  strokeColor = ""
		,          numeric lineWidth   = 0
		,          array   lineDash    = []
	) {
		var overlay = {};

		overlay.points = arguments.points;
		if ( Len( arguments.strokeColor ) ) {
			overlay.strokeColor = arguments.strokeColor;
		}
		if ( Int( arguments.lineWidth ) > 0 ) {
			overlay.lineWidth = Int( arguments.lineWidth );
		}
		if ( ArrayLen( arguments.lineDash ) ) {
			overlay.lineDash = arguments.lineDash;
		}

		variables.overlays = variables.overlays ?: [];
		ArrayAppend( variables.overlays, overlay );

		return this;
	}


// GETTERS AND SETTERS
	private any function _getSnapshotService() {
	    return _snapshotService;
	}
	private void function _setSnapshotService( required any snapshotService ) {
	    _snapshotService = arguments.snapshotService;
	}

}