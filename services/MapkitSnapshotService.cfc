/**
 * @presideService
 * @singleton
 */
component {

	property name="encodingUtils"         inject="encodingUtils@jwtcfml";
	property name="authenticationService" inject="MapkitAuthenticationService";

// CONSTRUCTOR
	/**
	 *
	 */
	public any function init() {
		return this;
	}

	public MapkitSnapshot function newSnapshot() {
		return new MapkitSnapshot( this );
	}

	public string function buildSnapshotUrl() {
		var event          = $getRequestContext();
		var mapkitSettings = $getPresideCategorySettings( "mapkitjs" );
		var params         = "";
		var value          = "";
		var expires        = DateAdd( "n", 20, Now() );
		var snapshotArgs   = duplicate( arguments );

		snapshotArgs.expires = encodingUtils.convertDateToUnixTimestamp( expires );

		for( var arg in snapshotArgs ) {
			value = snapshotArgs[ arg ];
			if ( isSimpleValue( value ) ) {
				params &= "#arg#=#value#&";
			} else {
				params &= "#arg#=#encodeForURL( serializeJson( value ) )#&";
			}
		}

		var endpoint       = "/api/v1/snapshot?#params#teamId=#mapkitSettings.team_id#&keyId=#mapkitSettings.key_id#";
		var signature      = authenticationService.signES256( endpoint, mapkitSettings.private_key );

		return "https://snapshot.apple-mapkit.com#endpoint#&signature=#signature#";
	}

	public any function getSnapshotBinary( required string snapshotUrl ) {
		var imageRequest = "";

		http url=arguments.snapshotUrl result="imageRequest" encodeurl=false;

		return imageRequest.filecontent;
	}

}